from django.urls import path
from .views import new,home,emi,inter,rest,food,ordered,paragon_cartdet,sagar_cartdet,rahmath_cartdet,salkara_cartdet,usercart_pay,addre,ends,confirm_address

urlpatterns = [
    path('', home, name='hm'),
    path('user/', new, name='use'),
    path('email/',emi, name='em'),
    path('intro/',inter, name='int'),
    path('listr/',rest, name='lrs'),
    path('listf/',food, name='lfi'),
    path('order/',ordered, name='ord'),
    path('paragon/',paragon_cartdet, name='par'),
    path('sagar/',sagar_cartdet, name='sag'),
    path('rahmath/',rahmath_cartdet, name='rah'),
    path('salkara/', salkara_cartdet, name='salk'),
    path('cart/', usercart_pay, name='pa'),
    path('new_address/', addre, name='ad'),
    path('end/', ends, name='en'),
    path('address_confirm/', confirm_address, name='ca'),
]
from django.shortcuts import render,redirect
from .models import newreg,product,cart
from .form import user,addr,cartForm
from django.contrib.auth.hashers import check_password
from eatupproject import settings
from django.core.mail import send_mail
from eatupproject.settings import EMAIL_HOST_USER
from django.db.models import Sum

def new(request):
    s=user()
    if request.method == "POST":

        if s ==" ":
            text={'form':s}
            return render(request,'user.html',text)
        else:
            s=user(request.POST)
            subject="Welcome Eat Up!!!"
            message="You account has been created successfully!EAT UP!!!"
            recepient=str(s['Email'].value())
            send_mail(subject,message,EMAIL_HOST_USER,[recepient],fail_silently=False)
            if s.is_valid():
                s.save()
            st=newreg.objects.all()
            t = {'recepient': recepient, 'form': s, 'stu': st}
            return render(request, 'email.html', t)
    text={ 'form':s }
    return render(request, 'user.html', text)

def home(request):
    if request.method == 'GET':
        return render(request,'home.html')
    else:
        uname = request.POST.get('username')
        password = request.POST.get('password')
        customer = newreg\
            .get_user_by_username(uname)
        print(customer)
        print(uname,password)
        error_message = None
        if customer:
            f=check_password(password,customer.password)
            print(customer.password)
            if password == customer.password:
                return redirect('int')
            else:
                error_message = 'Username or Password invalid !!'
        else:
            error_message = 'Username or Password invalid !!'

        return render(request,'home.html',{'error':error_message})

def emi(request):
    return render(request,'email.html')
def inter(request):
    return render(request,'intro.html')
def rest(request):
    return render(request,'restaurnts.html')
def food(request):
    return render(request,'food_items.html')
def ordered(request):
    return render(request,'order.html')
def ends(request):
    return render(request,'last.html')
def confirm_address(request):
    return render(request,'address_confirm.html')

def addre(request):
    p = addr()
    if request.method == 'POST':
        if p == " ":
            text = {'form': p}
            return render(request, 'new_address.html', text)
        else:
            p = addr(request.POST)
            subject = "Address confirmed"
            message = "Hi!!!Address is created successfully...Healthy eating is a way of life, so it’s important to establish routines that are simple, realistically, and ultimately livable-Eat Up!"
            recepient = str(p['Email'].value())
            send_mail(subject, message, EMAIL_HOST_USER, [recepient], fail_silently=False)
            if p.is_valid():
                p.save()
            n = {'recepient': recepient, 'form': p}
            return render(request, 'address_confirm.html', n)
    text = {'form': p}
    return render(request, 'new_address.html', text)

def paragon_cartdet(request):
    # redirect('studntdet')
    c = cartForm()
    w = request.POST.get('restaurant')
    # cartlist = defaultdict(list)
    # qntylist = defaultdict(list)

    if request.method == 'POST':

        # usr = request.user.username if request.user else  redirect('')
        paragon = request.POST.get('restaurant', None)
        q = request.POST.get('quantity')

        selected_item=product.get_item_by_name(paragon)
        # if paragon in ["Non Veg. Ghee Rice Thali", "Veg. Meals Thali", "Boiled Rice"]:

            # cartlist[usr].append(w)
            # qntylist[usr].append(q)

        if selected_item:
            item=paragon
            quantity=q
            price=float(q)*(selected_item.price)
            print(item,quantity,price)
            cartitem = cart(item=item, quantity=quantity, price=price)
            cartitem.save()

        total_price=cart.objects.aggregate(Sum('price'))
        print(total_price)
        # item = cartlist
        # num = qntylist
        # text = {'items':item,'quantity':quantity,'price':price,'total_price':total_price}
        return render(request, 'paragon.html')
    return render(request, 'paragon.html')

def usercart_pay(request):
    user_cart=cartForm()
    items_all = cart.objects.all()
    total_price = sum(items_all.values_list('price', flat=True))
    # total_price = cart.objects.aggregate(Sum('price'))
    print(total_price)
    if request.method=='POST':
         object = request.POST.get('item',None)
         cart.objects.filter(item=object).delete()
         return redirect('pa')
    all_items=cart.objects.all()
    text={'form':user_cart,'total_price':total_price,'all_items':all_items}
    return render(request,'cart.html',text)

def sagar_cartdet(request):
    # redirect('studntdet')
    c = cartForm()
    w = request.POST.get('restaurant_1')
    # cartlist = defaultdict(list)
    # qntylist = defaultdict(list)

    if request.method == 'POST':

        # usr = request.user.username if request.user else  redirect('')
        sagar= request.POST.get('restaurant_1', None)
        q = request.POST.get('quantity_1')
        selected_item=product.get_item_by_name(sagar)
        # if paragon in ["Non Veg. Ghee Rice Thali", "Veg. Meals Thali", "Boiled Rice"]:

            # cartlist[usr].append(w)
            # qntylist[usr].append(q)

        if selected_item:
            item=sagar
            quantity_1=q
            price=float(q)*(selected_item.price)
            print(item,quantity_1,price)
            cartitem = cart(item=item, quantity=quantity_1, price=price)
            cartitem.save()

        total_price=cart.objects.aggregate(Sum('price'))
        print(total_price)
        # item = cartlist
        # num = qntylist
        # text = {'items':item,'quantity':quantity,'price':price,'total_price':total_price,'t0':t0}
        return render(request, 'sagar.html')
    return render(request, 'sagar.html')

def rahmath_cartdet(request):
    # redirect('studntdet')
    c = cartForm()
    w = request.POST.get('restaurant_2')
    # cartlist = defaultdict(list)
    # qntylist = defaultdict(list)

    if request.method == 'POST':

        # usr = request.user.username if request.user else  redirect('')
        rahmath= request.POST.get('restaurant_2', None)
        q = request.POST.get('quantity_2')
        selected_item=product.get_item_by_name(rahmath)
        # if paragon in ["Non Veg. Ghee Rice Thali", "Veg. Meals Thali", "Boiled Rice"]:

            # cartlist[usr].append(w)
            # qntylist[usr].append(q)

        if selected_item:
            item=rahmath
            quantity_2=q
            price=float(q)*(selected_item.price)
            print(item,quantity_2,price)
            cartitem = cart(item=item, quantity=quantity_2, price=price)
            cartitem.save()

        total_price=cart.objects.aggregate(Sum('price'))
        print(total_price)
        # item = cartlist
        # num = qntylist
        # text = {'items':item,'quantity':quantity,'price':price,'total_price':total_price}
        return render(request, 'rahmath.html')
    return render(request, 'rahmath.html')

def salkara_cartdet(request):
    # redirect('studntdet')
    c = cartForm()
    w = request.POST.get('restaurant_3')
    # cartlist = defaultdict(list)
    # qntylist = defaultdict(list)

    if request.method == 'POST':

        # usr = request.user.username if request.user else  redirect('')
        salkara= request.POST.get('restaurant_3', None)
        q = request.POST.get('quantity_3')
        selected_item=product.get_item_by_name(salkara)
        # if paragon in ["Non Veg. Ghee Rice Thali", "Veg. Meals Thali", "Boiled Rice"]:

            # cartlist[usr].append(w)
            # qntylist[usr].append(q)

        if selected_item:
            item=salkara
            quantity_3=q
            price=float(q)*(selected_item.price)
            print(item,quantity_3,price)
            cartitem = cart(item=item, quantity=quantity_3, price=price)
            cartitem.save()

        total_price=cart.objects.aggregate(Sum('price'))
        print(total_price)
        # item = cartlist
        # num = qntylist
        # text = {'items':item,'quantity':quantity,'price':price,'total_price':total_price}
        return render(request, 'salkara.html')
    return render(request, 'salkara.html')

from __future__ import unicode_literals
from django.db import models

class newreg(models.Model):
    Name=models.CharField(max_length=20)
    Address = models.CharField(max_length=70)
    Email=models.EmailField()
    Phno=models.IntegerField()
    username = models.CharField(max_length=20)
    password= models.CharField(max_length=20)
    def __str__(self):
        return self.username

    class Meta:
        db_table = "newreg"


    def register(self):
        self.save()

    @staticmethod
    def get_user_by_username(username):
        try:
            return newreg.objects.get(username=username)
        except:
            return False

    def isExists(self):
        if newreg.objects.filter(username=self.username):
            return True
        return False

class add(models.Model):
    Full_Name= models.CharField(max_length=20)
    Phone_Number= models.IntegerField()
    Pincode= models.IntegerField()
    City= models.CharField(max_length=20)
    Address= models.CharField(max_length=20)
    Email= models.EmailField()

    def __str__(self):
        return self.Email

    class Meta:
        db_table = "add"

class product(models.Model):
    category= models.CharField(max_length=20)
    item= models.CharField(max_length=20)
    quantity= models.FloatField()
    price= models.FloatField()

    def __str__(self):
        return self.item

    class Meta:
        db_table = "product"


    @staticmethod
    def get_item_by_name(item):
        try:
            return product.objects.get(item=item)
        except:
            return False

    def isExists(self):
        if product.objects.filter(item=self.item):
            return True
        return False

class cart(models.Model):
    category= models.CharField(max_length=20)
    item= models.CharField(max_length=20)
    quantity= models.FloatField()
    price= models.FloatField()

    def __str__(self):
        return self.item

    class Meta:
        db_table = "cart"

    @staticmethod
    def get_item_to_delete(item):
        try:
            return cart.objects.get(item=item)
        except:
            return False

    def isExists(self):
        if cart.objects.filter(item=self.item):
            return True
        return False



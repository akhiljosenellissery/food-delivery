from django import forms
from .models import newreg,add,product,cart

class user(forms.ModelForm):
    class Meta:
        model=newreg
        fields = "__all__"

class addr(forms.ModelForm):
    class Meta:
        model = add
        fields = "__all__"

class productForm(forms.ModelForm):
    class Meta:
        model = product
        fields = "__all__"

class cartForm(forms.ModelForm):
    class Meta:
        model = cart
        fields = "__all__"
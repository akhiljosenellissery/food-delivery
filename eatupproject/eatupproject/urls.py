"""eatupproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('myapp.urls')),
    path('user/', include('myapp.urls')),
    path('email/', include('myapp.urls')),
    path('intro/', include('myapp.urls')),
    path('listr/', include('myapp.urls')),
    path('listf/', include('myapp.urls')),
    path('order/', include('myapp.urls')),
    path('paragon/', include('myapp.urls')),
    path('sagar/', include('myapp.urls')),
    path('rahmath/', include('myapp.urls')),
    path('salkara/', include('myapp.urls')),
    path('cart/', include('myapp.urls')),
    path('new_address/', include('myapp.urls')),
    path('end/', include('myapp.urls')),
    path('address_confirm/', include('myapp.urls')),
]
